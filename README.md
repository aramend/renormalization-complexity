Solutions for problems in the "Introduction to Renormalization" course from
[Complexity Explorer](https://www.complexityexplorer.org/courses/67-introduction-to-renormalization).

Code-related solutions are written in Haskell. A PDF writeup of those
solutions is in `latex/solutions_jk.pdf`.

If you do not have your own setup to run such code, you can install the [Nix package manager](nixos.org). Don't worry, it will completely stay out of your system install.

Once that is done, enable Nix Flakes and
run
```
$ nix develop
```
in the root of this repository. Then you'll
have GHC etc available.