{
  description = "Code for complexity explorer exercises on renormalization.";
  inputs.nixpkgs.url = github:NixOS/nixpkgs/master;
  inputs.flake-utils.url = github:numtide/flake-utils;

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        haskellPackages = pkgs.haskellPackages;

        jailbreakUnbreak = pkg:
          pkgs.haskell.lib.doJaibreak (pkg.overrideAttrs (_: { meta = { }; }));

        packageName = "ceRenormalization";

        ghc = haskellPackages.ghcWithPackages (ps: with ps; [
          random
        ]);

        env = (with haskellPackages; [
          haskell-language-server
          cabal-install
          hlint
          ghcid
          cabal2nix
        ]);

      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            ghc
            env
          ];
        };
      });
}
