import System.Random
import Data.IORef

main :: IO ()
main = do
  let n = 10000
  r1Start <- randomIO :: IO RPSMove
  r2Start <- randomIO :: IO RPSMove
  href <- newIORef [playRound r1Start r2Start]
  mapM_
    (\_ -> runRobots href)
    [1..n]
  result <- (fmap . fmap) winner (readIORef href)
  let tieRatio = (length . filter (==0)) result
  let r1Ratio = (length . filter (==1)) result
  let r2Ratio = (length . filter (==2)) result
  putStrLn $ "ties: " ++ show (fromIntegral tieRatio / n)
  putStrLn $ "r1: " ++ show (fromIntegral r1Ratio / n)
  putStrLn $ "r2: " ++ show (fromIntegral r2Ratio / n)

data RPSMove = Rock | Paper | Scissors deriving (Eq, Show, Enum, Bounded)

instance Random RPSMove where
  randomR (a, b) g =
    case randomR (fromEnum a, fromEnum b) g of
      (x, g') -> (toEnum x, g')
  random g = randomR (minBound, maxBound) g

instance Ord RPSMove where
  compare Rock Scissors = GT
  compare Scissors Paper = GT
  compare Paper Rock = GT
  compare Scissors Rock = LT
  compare Paper Scissors = LT
  compare Rock Paper = LT
  compare _ _ = EQ

data RPSRound = RPSRound
  { robot1move :: RPSMove,
    robot2move :: RPSMove,
    winner :: Int
  }
  deriving (Eq, Show)

-- R1 sometimes plays random moves
-- R2 alway plays the best move
runRobots :: IORef [RPSRound] -> IO ()
runRobots href = do
  history <- readIORef href
  let last = head history
  r1Next <- strategy1 last
  let r2Next = strategy2 last
  writeIORef href (playRound r1Next r2Next : history)

playRound :: RPSMove -> RPSMove -> RPSRound
playRound r1Move r2Move = RPSRound r1Move r2Move wID
  where
    wID
      | r1Move == r2Move = 0
      | r1Move > r2Move = 1
      | otherwise = 2

-- The strategy for R1, sometimes play randomly
strategy1 :: RPSRound -> IO RPSMove
strategy1 lastRound = do
  roll <- randomRIO (0, 1) :: IO Float
  if roll < 0.5
    then randomIO :: IO RPSMove
    else return $ (bestMove . robot2move) lastRound

-- The strategy for R2, always play the optimal move
strategy2 :: RPSRound -> RPSMove
strategy2 = bestMove . robot1move

bestMove :: RPSMove -> RPSMove
bestMove Rock = Paper
bestMove Scissors = Rock
bestMove Paper = Scissors
