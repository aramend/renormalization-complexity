import Control.Monad ( forM_ )
import Control.Monad.ST ( ST, runST )
import Data.Array ( (!), bounds, elems, listArray, Array )
import Data.Array.ST
    ( STArray, writeArray, MArray(newArray), runSTArray )
import Data.STRef ( newSTRef, readSTRef, writeSTRef )
import Debug.Trace
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [ntext] -> do
      let n = read ntext :: Int
      printMatrix $ renormN n rpsMat
    _ -> error "Unable to parse input"

-- Data for stochastic matrix
type StocMat = Array (Int, Int) Rational

type PState = Array Int Rational

testState :: PState
testState = listArray (1, 2) [0.4, 0.6]

testMat2 :: StocMat
testMat2 = listArray ((1, 1), (2, 2)) [0.4, 0.2, 0.6, 0.8]

testMat3 :: StocMat
testMat3 = listArray ((1, 1), (3, 3)) [0.4, 0, 0.6, 0.6, 0.8, 0, 0, 0.2, 0.4]

rpsMat :: StocMat
rpsMat = listArray ((1, 1), (9, 9))
-- States given as (R1, R2) pairs
-- These state transitions go as column -> row
-- RR RP RS PR PP PS SR SP SS
  [
    0, 0, 0, 0, 0, 0, 1/6, 1/6, 2/3, -- RR
    1/6, 1/6, 2/3, 0, 0, 0, 0, 0, 0, -- RP
    0, 0, 0, 1/6, 1/6, 2/3, 0, 0, 0, -- RS
    0, 0, 0, 0, 0, 0, 2/3, 1/6, 1/6, -- PR
    2/3, 1/6, 1/6, 0, 0, 0, 0, 0, 0, -- PP
    0, 0, 0, 2/3, 1/6, 1/6, 0, 0, 0, -- PS
    0, 0, 0, 0, 0, 0, 1/6, 2/3, 1/6, -- SR
    1/6, 2/3, 1/6, 0, 0, 0, 0, 0, 0, -- SP
    0, 0, 0, 1/6, 2/3, 1/6, 0, 0, 0  -- SS
  ]

printMatrix :: StocMat -> IO ()
printMatrix smat = do
  let msize = matSize smat
  let toFloat x = fromRational x :: Float
  mapM_
    ( \(ix, val) ->
        if rem ix msize == 0
          then putStr $ (show . toFloat) val ++ "\n"
          else putStr $ (show . toFloat) val ++ "\t"
    )
    (zip [1 ..] (elems smat))

-- Coarse-grain stochastic matrix
renormN :: Int -> StocMat -> StocMat
renormN n smat = runST $ do
  arr <- newSTRef smat
  forM_
    [1 .. n]
    ( \_ -> do
        current <- readSTRef arr
        writeSTRef arr (renorm1 current)
    )
  readSTRef arr

renorm1 :: StocMat -> StocMat
renorm1 smat = runSTArray $ do
  let msize = matSize smat
  let ixs = [(i, j) | i <- [1 .. msize], j <- [1 .. msize]]
  newSmat <- newArray ((1, 1), (msize, msize)) 0 :: ST s (STArray s (Int, Int) Rational)
  mapM_ (\ij -> writeIndex ij newSmat smat msize) ixs
  return newSmat

writeIndex :: (Int, Int) -> STArray s (Int, Int) Rational -> StocMat -> Int -> ST s ()
writeIndex ij writeArr readArr msize = do
  let toSum = [readArr ! (fst ij, k) * readArr ! (k, snd ij) | k <- [1 .. msize]]
  writeArray writeArr ij (sum toSum)

matSize :: StocMat -> Int
matSize m = end - start + 1
  where
    end = (snd . snd . bounds) m
    start = (snd . fst . bounds) m

-- Apply stochastic matrix to probability state
applyStocMat :: StocMat -> PState -> PState
applyStocMat = undefined
