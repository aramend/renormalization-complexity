{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (when)
import Data.List
import Data.Word
import Foreign.Marshal.Utils (fromBool)
import System.Environment (getArgs)
import Text.Printf (printf)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [n] -> renormalizeRule (read n :: Word8)
    _ -> error "Expecting one argument, a uint8 rule number."

renormalizeRule :: Word8 -> IO ()
renormalizeRule leftRule = do
  let state = take 6 $ uint8ToRule 20
  mapM_
    ( \projNr -> do
        let commutesLeft state = return $ p projNr (runf2 leftRule state)
        mapM_
          ( \rightRule -> do
              let commutesRight state = return $ runG rightRule (p projNr state)
              resLeft <- mapM (commutesLeft . take 6 . uint8ToRule) [0 .. 63]
              resRight <- mapM (commutesRight . take 6 . uint8ToRule) [0 .. 63]
              when (resLeft == resRight) $ do
                let projRule = (showCEState . take 4 . uint8ToRule) projNr
                printf "Rule %d renormalizes with projection %s to rule %d.\n" leftRule projRule rightRule
          )
          $ filter (/= leftRule) [0 .. 255] -- All other rules except the target
    )
    [1 .. 14] -- All non-trivial projections

{-
RULE APPLICATIONS

Define functions for f, f2 and g
-}

runf2 ::
  -- | The rule number
  Word8 ->
  -- | The initial state
  [Bool] ->
  -- | The resulting supercell
  [Bool]
runf2 n = f2
  where
    f = fmap (applyRule (uint8ToRule n))
    f2 = f . substates 3 . f . substates 3

runG ::
  -- | The rule number
  Word8 ->
  -- | The initial state (triplet)
  [Bool] ->
  -- | The resulting singleton cell
  [Bool]
runG n = f . substates 3
  where
    f = fmap (applyRule (uint8ToRule n))

-- Select supercells of size s
substates :: Int -> [Bool] -> [[Bool]]
substates s = fmap (take s) . filter (\t -> length t >= 3) . tails

uint8ToRule :: Word8 -> [Bool]
uint8ToRule n = reverse $ pad ++ encoding
  where
    encoding = uint8ToBinary n
    pad = [False | _ <- [1 .. 8 - length encoding]]

applyRule :: [Bool] -> [Bool] -> Bool
applyRule rule substate = rule !! binaryToInt substate

{-
PROJECTIONS

Define the projection operator p
-}

p ::
  -- | The projection rule to use (1-14)
  Word8 ->
  -- | The fine-grained state to apply the projection operator on
  [Bool] ->
  -- | The projected (coarse-grained) state
  [Bool]
p n = fmap (applyRule rule) . supercells
  where
    rule = (take 4 . uint8ToRule) n

supercells :: [Bool] -> [[Bool]]
supercells (b : b' : bs) = [b, b'] : supercells bs
supercells [] = []
supercells _ = error "Encountered odd number of cells"

{-
Utility functions
-}

uint8ToBinary :: Word8 -> [Bool]
uint8ToBinary n
  | n > 0 = uint8ToBinary q ++ [r > 0]
  | otherwise = [r > 0]
  where
    (q, r) = quotRem n 2

-- Assume the Wolfram ordering
binaryToInt :: [Bool] -> Int
binaryToInt = sum . zipWith (\p b -> b * 2 ^ p) [0 ..] . fmap fromBool

showCState :: Bool -> String
showCState b
  | b = "1" -- "█"
  | otherwise = "0" -- " "

showCEState :: [Bool] -> String
showCEState = concatMap showCState