\documentclass{article}
\usepackage{amsmath}
\usepackage{cleveref}
\usepackage{listings}
\usepackage[margin=1cm, bmargin=2cm]{geometry}
\usepackage{tikz-cd}
\usepackage{biblatex}
\addbibresource{cite.bib}

\title{Solutions to Renormalization Course Problem Set}
\author{Jaan Kasak}

\begin{document}
  \noindent
  The following comprise my solutions to the renormalization course problem set.
  The code used for these can be found in the \verb+src/+ folder.
  I used the Haskell language for these solutions, both as an exercise in learning
  it, but also due to the similarly general theory it is based on, which also finds
  use in complexity research (category theory).

  \section{Introduction to coarse-graining}
  Solutions can be found in \verb+src/Introduction.hs+. Assuming I copied over
  the probabilities in the handout correctly, they sum to 0.993827. The difference
  probably arises from rounding errors. Close enough!
  \subsection*{Q1}
  In units of bits, the entropy is $H(\text{charProbs}) = 4.1798177$.
  \subsection*{Q2}
  The probabilities in the (now Bernoulli) distribution are 0.37953153 for the vowels,
  and 0.6198512 for the not-vowels. This leads to an entropy of 0.95817554 bits, which
  is a lot less than for the previous case. Which makes sense, since now the only thing
  I can be uncertain about is whether or not something is a vowel, instead of what the actual
  letter should be, where I had a lot more possibility. I don't have an entropy
  of 1 bit perfectly, which would reflect a balanced binary yes/no, 0/1 choice.
  Since the distribution is slightly shifted towards sampling a not-vowel, I am less uncertain
  about the end result.
  \subsection*{Q3}
  The lengths of the example sentence (in words) and corresponding course-grained time-series
  do not match. If I were to guess, then the mistake is around the word fortune, which should be a
  \verb+NOUN+, not a \verb+VERB+. \\

  \noindent
  Regardless, syntactical rules of English apply in this coarse-graining. English is a fun
  language with more exceptions than rules, but in broad strokes we would still find things
  like \verb+ADJECTIVE+s being usually followed by \verb+NOUN+s, and \verb+ADVERB+s implying
  the presence of a \verb+VERB+ somewhere else in the sentence.

  \section{Renormalizing Markov Chains}
  \subsection*{Q1}
  The two-state slippy counter has one free parameter $\epsilon$, and its
  stochastic matrix is defined as
  \begin{gather}
    T = 
    \begin{pmatrix}
      \epsilon & 1 - \epsilon \\
      1 - \epsilon & \epsilon \\
    \end{pmatrix}
  \end{gather}
  For there to exist a Markov chain that is a microtheory of the slippy
  counter, say with a resolution of one extra step, we expect another
  stochastic matrix $A$ to exist.
  \begin{gather}
    \label{eq:microtheory}
    A =
    \begin{pmatrix}
      a & 1 - b \\
      1 - a & b \\
    \end{pmatrix},
    \qquad
    A^2 = T
  \end{gather}
  \begin{align}
    A^2 &=
    \begin{pmatrix}
      a & 1 - b \\
      1 - a & b \\
    \end{pmatrix}
    \begin{pmatrix}
      a & 1 - b \\
      1 - a & b \\
    \end{pmatrix} \nonumber \\
    &=
    \begin{pmatrix}
      a^2 + (1-a)(1-b) & - \\
      - & b^2 + (1-a)(1-b) \\
    \end{pmatrix}
  \end{align}
  \begin{gather}
    \begin{pmatrix}
      a^2 + (1-a)(1-b) & - \\
      - & b^2 + (1-a)(1-b) \\
    \end{pmatrix}
    = 
    \begin{pmatrix}
      \epsilon & 1 - \epsilon \\
      1 - \epsilon & \epsilon \\
    \end{pmatrix}
    \ \Rightarrow
    \begin{cases}
      a^2 + (1-a)(1-b) = \epsilon \\
      b^2 + (1-a)(1-b) = \epsilon
    \end{cases}
  \end{gather}
  As one can see, $a = b$ for these equations to be satisfied. Then,
  only the solving of a quadratic equation remains.
  \begin{align}
    \epsilon &= a^2 + (1-a)^2 = 2a^2 - 2a + 1 \nonumber \\
    &\text{The discriminant here is} \nonumber \\
    D &= \sqrt{2^2 - 4 \times 2 \times 1} = \sqrt{-4}
  \end{align}
  The value of $\epsilon$ would end up an imaginary number, for \cref{eq:microtheory}
  to be satisified. In that case, $T$ would no longer be a valid stochastic
  matrix, an imaginary probability carries no meaning. The only conclusion
  then is that there exists no finer theory for which the slippy counter $T$
  would be a course-grained theory.

  \subsection*{Q2}
  I choose the matrix $A$ as my 3-state stochastic matrix, defined thusly.
  \begin{gather}
    A = 
    \begin{pmatrix}
      0.4 & 0 & 0.6 \\
      0.6 & 0.8 & 0 \\
      0 & 0.2 & 0.4 \\
    \end{pmatrix}
  \end{gather}
  Next we solve for the stationary distribution $p$, satisfying $Ap = p$.
  The computational solution is available in \verb+src/Markov.hs+.
  Running \verb+renormN 20 testMat3+ in GHCI reveals that all columns of $A$
  become
  \begin{gather}
    \begin{pmatrix}
      0.2 \\
      0.6 \\
      0.2 \\
    \end{pmatrix}.
  \end{gather}
  The analyitcal solution follows.
  \begin{gather}
    \begin{pmatrix}
      0.4 & 0 & 0.6 \\
      0.6 & 0.8 & 0 \\
      0 & 0.2 & 0.4 \\
    \end{pmatrix}
    \begin{pmatrix}
      p_1 \\
      p_2 \\
      p_3 \\
    \end{pmatrix}
    =
    \begin{pmatrix}
      p_1 \\
      p_2 \\
      p_3 \\
    \end{pmatrix}
    \ \Rightarrow
    \begin{cases}
      p_1 + p_2 + p_3 = 1 \\
      0.4 p_1 + 0.6 p_3 = p_1 \\
      0.6 p_1 + 0.8 p_2 = p_2 \\
      0.2 p_2 + 0.4 p_3 = p_3
    \end{cases}
    \ \Rightarrow
    p = 
    \begin{pmatrix}
      0.2 \\
      0.6 \\
      0.2 \\
    \end{pmatrix}
  \end{gather}

  \subsection*{Q3}
  The code for the solution is mostly available in \verb+src/RPS.hs+.
  Robot 1 implements the sometimes-random strategy, where with probability
  $p$ it will play a random move, and with probability $1-p$ play the
  optimal move. Robot 2 implements a strategy where it always plays the
  optimal move.

  Running the code reveals that the two robots are tied, one does not
  gain the upper hand over the other, seemingly irrespectively of the
  value for $p$.
  The stochastic matrix for this process is also a fairly simple one.
  It is a $9\times 9$ matrix defining the state transitions
  \begin{gather}
  (R_1, R_2) \rightarrow (\text{strategy}_1(R_2),\ \text{strategy}_2(R_1)).
  \end{gather}
  This matrix can be inhabited by only 3 possible values. Robot 2 follows
  the deterministic strategy$_2$, so all states unreachable by it
  have a probability 0 of occurring next.
  Among the non-zero next states, the values
  are either $p/3$ for when the choice could only have been arrived
  at randomly, and $(3-2p)/3$ for the optimal move that Robot 1 sometimes
  favours. I have written up an example of this matrix for $p=1/2$ in
  \verb+src/Markov.hs+ as the variable \verb+rpsMat+.
  Reusing code from the previous exercise, \verb+renormN 10 rpsMat+
  shows that in the limit, all values in the matrix are $1/9$.
  If all states are equally likely, then it is understandable that we
  did not observe a sure winner.

  \setcounter{section}{3}
  \section{Renormalizing Cellular Automata}
  \subsection*{Q1}
  All the relevant code is in \verb+src/CE.hs+. This was fun to code!
  We essentially aim to solve for projections $P$ and evolution rules
  $g$ that commute via
  \begin{gather}
    P \circ f \circ f = g \circ P
  \end{gather}
  for a chosen rule $f$.
  I first started by checking the renormalization from rule 105 to rule
  150 through all possible values for $P$, as covered in the lectures.
  \begin{lstlisting}
    $ ghci
    Prelude> :l src/CE.hs
    [1 of 1] Compiling Main                   ( src/CE.hs, interpreted )
    Ok, one module loaded.
    *Main> renormalizeRule 105
    Rule 105 renormalizes with projection 1100 to rule 150.
    Rule 105 renormalizes with projection 1010 to rule 150.
    Rule 105 renormalizes with projection 0110 to rule 150.
    Rule 105 renormalizes with projection 1001 to rule 150.
    Rule 105 renormalizes with projection 0101 to rule 150.
    Rule 105 renormalizes with projection 0011 to rule 150.
  \end{lstlisting}
  We see that there are actually six (non-trivial) projections
  that make this viable.
  It was easy enough to extend this code to search across all $g$ rules
  as well, so it does just that. The program is still only considering
  non-trivial projections, and we don't coarse-grain a rule to itself
  ($g$ can't be $f$). We are also still only searching for coarse-grainings
  using supercells of size 2, which corresponds to $N=2$ in \cite{israeli2004}.
  \begin{lstlisting}
    *Main> renormalizeRule 90
    Rule 90 renormalizes with projection 1100 to rule 165.
    Rule 90 renormalizes with projection 1010 to rule 165.
    Rule 90 renormalizes with projection 1001 to rule 165.
  \end{lstlisting}
  
  \setcounter{section}{5}
  \section{Renormalizing the Creature (Krohn-Rodes Theorem)}
  \label[section]{c6}
  \subsection*{Q1}
  The coarse-graining of $Z_6$ into two states can be accomplished by
  naming the two states $E$ (even, contains states 2,4,6) and $O$
  (odd, contains states 1,3,5). The operator $C$ is cyclic between
  $E$ and $O$, always moving from one into the other.
  \begin{figure}[h]
    \centering
    \begin{tikzcd}
      E \arrow[r, bend left=50, "C"] & O \arrow[l, bend left=50, "C"]
    \end{tikzcd}
  \end{figure}

  \subsection*{Q2}
  To sort the fine-grained states $\{1,2,..,36\}$ in $Z_{36}$, we need only look
  to the modulus operator.
  I can take the course-grained states $A$, $B$ and $C$ to be
  filled by fine-grained states according to their remainder upon division
  by 3. The possible values are 0, 1, or 2. Correspondingly, the fine-grained
  states are $\{3,6,..,33,36\}$, $\{1,4,..,31,34\}$ and $\{2,5,..,32,35\}$.
  We end up with a group that looks the same as $Z_3$.
  \begin{figure}[h]
    \centering
    \begin{tikzcd}[row sep=tiny]
                   & B \arrow[dd] \\
      A \arrow[ur] &              \\
                   & C \arrow[ul]
    \end{tikzcd}
  \end{figure}

  \subsection*{Q3}
  The size of the cyclic group modulo some number $N$ is what helped use
  in the last two questions. Here we run into the fact that 13 is a prime
  number, so there cannot be any course-graining besides the trivial one.
  If I were to choose modulo 3 or modulo 4 to create the coarse-graining,
  then it would work for near all the cases, except the one where it
  suddenly does not. In the first case, the arrow from the coarse-grained
  state containing 12 goes to the coarse-grained state containing 13,
  but in the next iteration the arrow loops back into 1, which is in the
  same coarse state as 13.

  \setcounter{section}{7}
  \section{Rate-Distortion Theory: Keeping the Things that Matter}
  A second coarse-graining of $Z_6$ can be created, by taking the modulus
  3 of each of the six states. Call the coarse-grained states $A'$,
  $B'$ and $C'$ again, and say contain numbers with remained 0, 1 and 2,
  in the same order. Then state $A'$ contains the fine-grained states 
  $\{6, 3\}$, $B'$ contains $\{1, 4\}$ and $C'$ contains $\{2, 5\}$.
  The arrows flow as in the diagram below.
  \begin{figure}[h]
    \centering
    \begin{tikzcd}[row sep=tiny]
                   & B' \arrow[dd] \\
      A' \arrow[ur] &              \\
                   & C' \arrow[ul]
    \end{tikzcd}
  \end{figure}

  \newpage
  \printbibliography
  \subsection*{Q1}
  If I write what I believe as the columns, and the reality is as the
  rows, then a distortion matrix leading me to the coarse-graining
  discovered in Q1 of \cref{c6} could be the following.
  \begin{gather}
    \begin{pmatrix}
      0 & 1 & 0 & 1 & 0 & 1 \\
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 0 & 1 & 0 & 1 \\
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 0 & 1 & 0 & 1 \\
      1 & 0 & 1 & 0 & 1 & 0
    \end{pmatrix}
  \end{gather}
  Here there is no penalty for correctly interpreting the evenness of the 
  fine-grained state, but a penalty of 1 for getting it wrong.
  \subsection*{Q2}
  Two prefer a 3 state coarse-graining of $Z_6$ we need only add more penalties.
  \begin{gather}
    \begin{pmatrix}
      0 & 1 & 1 & 0 & 1 & 1 \\
      1 & 0 & 1 & 1 & 0 & 1 \\
      1 & 1 & 0 & 1 & 1 & 0 \\
      0 & 1 & 1 & 0 & 1 & 1 \\
      1 & 0 & 1 & 1 & 0 & 1 \\
      1 & 1 & 0 & 1 & 1 & 0
    \end{pmatrix}
  \end{gather}
  In both cases the matrices are symmetric as well, so our ordering of belief
  and truth did not really matter.


\end{document}